%Cálculo de potencia relativa al peso del ciclista o de ciclista más equipo: 0/1
potrel = 0;
%Datos de la subida. Matriz de tres o cuatro columnas (podemos poner varias
%filas para calcular potencias para varios tiempos de subida, por ejemplo)
%Longitud (m), desnivel (m), VAM (m/h) o tiempo (minutos y segundos)
ldv = [9080.0 646.0 32 0];
%Parámetros atmosféricos para estimar la densidad del aire, que entra en el
%cálculo del efecto aerodinámico
%Puede ser una matriz de dos o tres columnas o un vector COLUMNA (o un escalar)
%   Vector columna: altitud media, en metros
%   Matriz (cada fila): [temperatura(ºC, oblig.) presión(mb, oblig.) humedad(%)]
atmosfera = 1400.0;
%Coeficientes de rodadura y aerodinámico
%Matriz de dos columnas (podemos poner varias filas para calcular potencias para
%varios valores de Crr y CdA). Cada columna contiene:
%   Crr: 0.0031->buen asfalto, 0.008->asfalto malo (me parece mucho este dato)
%        0.0030 sale para las Conti GP5000 de 25 mm a 8.3 bar en las pruebas de
%        https://www.bicyclerollingresistance.com/road-bike-reviews/continental-grand-prix-5000-2018
%   CdA: 0.4->arriba, 0.35->manetas, 0.31->abajo
%        Internamente NO se modifica este valor venga de donde venga el viento
cra = [0.0032 0.315];
%Matriz de dos columnas con el peso del ciclista (kg) y de la bici con el resto
%de achiperres como ropa, casco, zapatillas, bidones, herramientas, etc. (kg)
%Podemos poner varias filas para calcular potencias para varios pesos
pesos = [59.0 10.3];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Acimut (ángulo medido a derechas con respecto al norte geográfico) de la
%trayectoria de avance del ciclista (grados sexagesimales, formato decimal)
%Si se pasa el valor NaN no se tiene en cuenta ni este dato ni el del viento
%Puede ser un vector COLUMNA con múltiples valores
acimut = NaN;
%Acimut del viento (de dónde PROVIENE, en grados sexagesimales, formato decimal)
%y su velocidad, en km/h
%Si se pasa un vector vacío o de un solo elemento no se tiene en cuenta ni este
%dato ni el del acimut de la trayectoria del ciclista
%Puede ser una matriz con múltiples valores
%Este script SÓLO tiene en cuenta la componente del viento paralela a la
%trayectoria del ciclista. La componente lateral se descarta
viento = [];
%Velocidades de la bici inicial y final, en km/h
%Este dato se usa para calcular el cambio de energía cinética y no tiene mucho
%sentido más que para esfuerzos cortos y si los datos son buenos. Si se almacena
%NaN o vector vacío no se tiene en cuenta
%Puede ser una matriz con múltiples valores
vif = [];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%NOTAS
%
%De todas las variables que pueden recibir múltiples valores (filas de la matriz
%o del vector columna), sólo puede hacerlo una a la vez. Es decir, no podemos
%configurar por ejemplo dos filas en 'ldv' y también dos filas en 'acimut'
%
%Para la aerodinámica la ventaja de los ciclistas pequeños (bajos) es su menor
%CdA, aunque si la persona es delgada y alta lo tiene más difícil, ya que su
%superficie corporal sigue siendo relativamente alta. Para igualar la potencia
%relativa debida a este apartado es necesario tener un CdA hasta 0.1 menor que
%alguien grande (entendiendo por grande a un individuo que pese unos 20 kg más)
%
%Podemos estimar nuestro CdA con la herramienta Aerolab de GoldenCheetah
%
%En la energía potencial los flacos también salimos perdiendo en términos
%relativos aunque parezca mentira. En el cálculo de la diferencia de potencial
%entran el peso del ciclista y el de la bici, pero al calcular la potencia
%relativa sólo se divide entre el peso del ciclista (es así como
%tradicionalmente se da el dato), por lo que en ese sentido es peor para alguien
%que pese menos. Por eso creo que la potencia relativa debería darse teniendo en
%cuenta el peso del corredor más el de la bicicleta, pero eso, como tantas otras
%cosas que se empiezan haciendo de una determinada manera, no va a cambiar. De
%todas formas, creo que a un gordo le cuesta más sacar la misma potencia
%relativa que a un flaco debido a que (además de cuestiones fisiológicas del
%tipo de músculo) su peso de más se reparte por todo su cuerpo, no sólo en
%músculo en las piernas, por lo que acarrea más lastre
%
%FÓRMULA DE MARTIN (http://veloclinic.com/intro-to-power-estimate-modelling/)
%[1]: Validation of a Mathematical Model for Road Cycling Power
%doi: 10.1123/jab.14.3.276
%
%[2]: Drag and Side Force Analysis on Bicycle Wheel-Tire Combinations
%doi: 10.1115/1.4039513
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Gravedad media en la superficie de la Tierra
grav = 9.80665;
%Eficiencia de la transmisión, sacado de [1], pág. 8
et = 0.97698;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Extracción de datos de las matrices de entrada
lon = ldv(:,1);
desnivel = ldv(:,2);
velocidad = ldv(:,3:end);
pc = pesos(:,1);
pb = pesos(:,2);
crr = cra(:,1);
cda = cra(:,2);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Ángulo de pendiente y pendiente en porcentaje
ap = asin(desnivel./lon);
pp = tan(ap)*100.0;
%Calculamos la velocidad de ascensión vertical
if size(velocidad,2)==1
    %Velocidad de ascensión vertical y tiempo empleado en la subida (horas)
    vam = velocidad;
    t = desnivel./vam;
elseif size(velocidad,2)>=2
    %Tiempo empleado en la subida (horas) y velocidad de ascensión vertical
    t = velocidad(:,1)/60.0+velocidad(:,2)/3600.0;
    vam = desnivel./t;
else
    error('La definición de la velocidad en ''ldv'' es incorrecta');
end
%Velocidad de desplazamiento de la bici (m/s)
vm = lon./(t*3600.0);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Densidad del aire
if size(atmosfera,2)==1
    %Para el cálculo de la densidad del aire en función de la altitud uso la
    %formulación de https://en.wikipedia.org/wiki/Density_of_air#Troposphere,
    %que es la misma que se usa en
    %http://www.wind101.net/air-density/air-density-calculator.htm (para que
    %salgan los mismos resultados se ha de usar el valor 0 en el apartado de
    %presión y la temperatura ha de ser calculada a la altura de trabajo)
    %Esta formulación es distinta a la presentada en
    %http://veloclinic.com/intro-to-power-estimate-modelling/, que no pone de
    %dónde la saca, pero da resultados que, si no idénticos, no introducen
    %variaciones significativas en el cálculo de la potencia
    roa_p0 = 101325.0; %Presión atmosférica estándar al nivel del mar, en Pa
    roa_T0 = 288.15;   %Temperatura estándar al nivel del mar, en K
    roa_L = 0.0065;    %Decaimiento de la temperatura con la altitud, en K/m
    roa_R = 8.31447;   %Constante de los gases ideales, en J/(mol*K)
    roa_M = 0.0289654; %Masa molar del aire seco, en kg/mol
    %Temperatura a la altitud de trabajo
    roa_T = roa_T0-roa_L*atmosfera;
    %Presión a la altitud de trabajo
    roa_p = roa_p0*(1.0-roa_L*atmosfera./roa_T0).^(grav*roa_M/(roa_R*roa_L));
    %Densidad del aire
    rho_a = roa_p*roa_M./(roa_R*roa_T);
elseif size(atmosfera,2)>=2
    %Constante específica de los gases para el aire seco, en J/(kg*K)
    roa_Rd = 287.058;
    %Temperatura, en K
    roa_T = atmosfera(:,1)+273.15;
    %Presión atmosférica, en Pa
    roa_p = atmosfera(:,2)*100.0;
    %Compruebo si hay humedad o no
    if size(atmosfera,2)==2
        %Formulación de https://en.wikipedia.org/wiki/Density_of_air#Dry_air
        %Da los mismos resultados que los que se pueden obtener en
        %http://www.wind101.net/air-density/air-density-calculator.htm poniendo
        %la humedad a cero
        %Densidad del aire
        rho_a = roa_p./(roa_Rd*roa_T);
    else
        %Formulación de https://en.wikipedia.org/wiki/Density_of_air#Humid_air
        %Da los mismos resultados que los que se pueden obtener en
        %http://www.wind101.net/air-density/air-density-calculator.htm
        %Presión de saturación de vapor, en Pa
        roa_psat = 6.1078*10.0.^(7.5*atmosfera(:,1)./(atmosfera(:,1)+237.3))*...
                   100.0;
        %Presión de vapor de agua, en Pa
        roa_pv = atmosfera(:,3)./100.0.*roa_psat;
        %Presión parcial del aire seco
        roa_pd = roa_p-roa_pv;
        %Constante específica de los gases para el vapor de agua, en J/(kg*K)
        roa_Rv = 461.495;
        %Densidad del aire húmedo
        rho_a = roa_pd./(roa_Rd.*roa_T)+roa_pv./(roa_Rv*roa_T);
    end
else
    error('La definición de la variable ''atmosfera'' es incorrecta');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Comprobamos si hay que calcular la influencia del viento
if (~isnan(acimut))&&(size(viento,2)>=2)
    %Ángulo entre el viento y la trayectoria, en grados sexagesimales
    alfa = rem(viento(:,1)+180.0,360.0)-acimut;
    %Componente de la velocidad del viento en la dirección de la bici, en km/h
    vv = viento(:,2).*cos(alfa*pi./180.0);
    %Componente de la velocidad del viento en m/s
    vv = vv./3.6;
    %Cambio el signo para que sea aditivo si el viento es en contra (la
    %velocidad relativa bici/viento es mayor) y sustractivo si es a favor
    vv = -vv;
else
    %Velocidad del viento igual a cero
    vv = 0.0;
end
%Ajustamos dimensiones
if isscalar(vv)
    %Las dimensiones de vv han de ser iguales a las de vv
    vv = ones(length(vam),1)*vv;
else
    %Las dimensiones de vam, vm y t han de ser iguales a las de vv
    vam = ones(length(vv),1)*vam;
    vm = ones(length(vv),1)*vm;
    t = ones(length(vv),1)*t;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Efecto de la aerodinámica, en W (ecuación 2 de [1])
w_aero = 0.5*cda.*rho_a.*(vm+vv).^2.*vm;
%Efecto aerodinámico de la rotación de la rueda (ecuación 3 de [1])
%Aquí el CdA se cambia por un parámetro que en el artículo de Martin pone que se
%midió ex profeso. Como yo no puedo hacerlo podría tomar el valor que usan
%ellos, que siempre será mejor que nada
%Sin embargo en [2] se indican valores de CdA para diferentes combinaciones de
%ruedas y cubiertas. En mi caso (rueda no aerodinámica con Continental) uso
%0.030 por cada rueda (0.06 en total para las dos ruedas, aunque no sé si la
%trasera y la delantera contribuyen igual)
w_aero_r = 0.5*0.06*rho_a.*(vm+vv).^2.*vm;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Resistencia a la rodadura (ecuación 6 de [1])
w_rr = vm.*cos(ap).*crr.*(pc+pb)*grav;
%Fricción de los rodamientos (ecuación 7 de [1])
w_r = vm.*(91.0+8.7*vm)*1.0e-3;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Efecto de la gravedad (cambio de energía potencial), ecuación 9 de [1]
w_ep = vm.*(pc+pb)*grav.*sin(ap);
%Velocidades inicial y final en m/s
if (~isempty(vif))&&(size(vif,2)>=2)&&all(~isnan(vif(:)))
    %Velocidades en m/s
    vini = vif(:,1)./3.6;
    vfin = vif(:,2)./3.6;
else
    %Asignamos 0
    vini = 0.0;
    vfin = 0.0;
end
%Incremento de tiempo en segundos
dt = t*3600.0;
%Efecto del cambio en energía cinética (ecuación 12 de [1])
%El momento de inercia del conjunto de ambas ruedas se aproxima en [1] por
%0.14 kg*m^2. Para las Zonda a mí me da 0.15563, que es el que usaré
%El radio exterior de las ruedas (cubierta incluida) es 0.345 m
w_ec = 0.5*(pc+pb+0.15563./(0.345^2)).*(vfin.^2-vini.^2)./dt;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Efecto aerodinámico completo
w_A = w_aero+w_aero_r;
%Efecto de rozamiento completo
w_R = w_rr+w_r;
%Efecto de variación de energía completo
w_E = w_ep+w_ec;
%Divido todos los efectos entre la eficiencia de la transmisión
w_A = w_A./et;
w_R = w_R./et;
w_E = w_E./et;
%Potencia total
w_T = w_A+w_R+w_E;
%Ajusto dimensiones
if ~isscalar(w_T)
    if isscalar(w_A)
        w_A = ones(length(w_T),1).*w_A;
    end
    if isscalar(w_R)
        w_R = ones(length(w_T),1).*w_R;
    end
    if isscalar(w_E)
        w_E = ones(length(w_T),1).*w_E;
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Factor de pendiente en la fórmula de Ferrari
fp = 2.0+pp./10.0;
%Potencia según la fórmula de Ferrari
w_F_r = vam./(fp*100.0);
%Compruebo si quiero los vatios relativos a ciclista más equipo
if potrel
    %Potencia absoluta
    w_F_r = w_F_r*pc;
    %Potencia relativa
    w_F_r = w_F_r./(pc+pb);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Potencia relativa al peso del ciclista más equipo o al peso del ciclista
if potrel
    w_A_r = w_A./(pc+pb);
    w_R_r = w_R./(pc+pb);
    w_E_r = w_E./(pc+pb);
    w_T_r = w_T./(pc+pb);
else
    w_A_r = w_A./pc;
    w_R_r = w_R./pc;
    w_E_r = w_E./pc;
    w_T_r = w_T./pc;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Resultados
fprintf(1,'*Datos:\n');
fprintf(1,'\tPeso ciclista: %.2f kg\n',pc(1));
for i=2:length(pc)
    fprintf(1,'\t               %.2f kg\n',pc(i));
end
fprintf(1,'\tPeso bici y equipo: %.2f kg\n',pb(1));
for i=2:length(pb)
    fprintf(1,'\t                    %.2f kg\n',pb(i));
end
fprintf(1,['\tCrr: %.4f, CdA: %.4f (la componente lateral del viento no ',...
           'modifica CdA)\n'],crr(1),cda(1));
for i=2:length(crr)
    fprintf(1,'\t     %.4f,      %.4f\n',crr(i),cda(i));
end
fprintf(1,'*Datos del trayecto:\n');
fprintf(1,'\tLongitud: %.3f km, pendiente media: %.2f%%\n',lon(1)/1000.0,pp(1));
for i=2:length(lon)
    fprintf(1,'\t          %.3f km,                  %.2f%%\n',...
            lon(i)/1000.0,pp(i));
end
fprintf(1,'*Velocidades:\n');
fprintf(1,['\tVAM: %.1f m/h, velocidad: %.2f km/h, tiempo: %d''%2d'''', ',...
           'viento: %.2f km/h\n'],...
        vam(1),vm(1)*3.6,floor(t(1)*60.0),...
        floor((t(1)*60.0-floor(t(1)*60.0))*60.0),-vv(1)*3.6);
for i=2:length(vam)
    fprintf(1,['\t     %.1f m/h,            %.2f km/h,         %d''%2d'''',',...
               '         %.2f km/h\n'],...
            vam(i),vm(i)*3.6,floor(t(i)*60.0),...
            floor((t(i)*60.0-floor(t(i)*60.0))*60.0),-vv(i)*3.6);
end
fprintf(1,'*Potencia relativa ');
if potrel
    fprintf(1,'al peso del ciclista más equipo:\n');
else
    fprintf(1,'al peso del ciclista:\n');
end
fprintf(1,['\tMartin:...... %.2f W/kg = %.2f+%.2f+%.2f W/kg ',...
           '(energía+aerodinámica+rodadura)\n'],...
           w_T_r(1),w_E_r(1),w_A_r(1),w_R_r(1));
for i=2:length(w_T_r)
    fprintf(1,'\t              %.2f W/kg = %.2f+%.2f+%.2f W/kg\n',...
            w_T_r(i),w_E_r(i),w_A_r(i),w_R_r(i));
end
fprintf(1,'\t              %4.0f W    = %4.0f+%4.0f+%4.0f W\n',...
        w_T(1),w_E(1),w_A(1),w_R(1));
for i=2:length(w_T_r)
    fprintf(1,'\t              %4.0f W    = %4.0f+%4.0f+%4.0f W\n',...
            w_T(i),w_E(i),w_A(i),w_R(i));
end
fprintf(1,'\tFerrari:..... %.2f W/kg\n',w_F_r(1));
for i=2:length(w_F_r)
    fprintf(1,'\t              %.2f W/kg\n',w_F_r(i));
end
%%%%%%%%%%%%%&%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Copyright (c) 2020-2022, J.L.G. Pallero, jgpallero@gmail.com
%
%All rights reserved.
%
%Redistribution and use in source and binary forms, with or without
%modification, are permitted provided that the following conditions are met:
%
%- Redistributions of source code must retain the above copyright notice, this
%  list of conditions and the following disclaimer.
%- Redistributions in binary form must reproduce the above copyright notice,
%  this list of conditions and the following disclaimer in the documentation
%  and/or other materials provided with the distribution.
%- Neither the name of the copyright holders nor the names of its contributors
%  may be used to endorse or promote products derived from this software without
%  specific prior written permission.
%
%THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
%ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
%WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
%DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
%INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
%BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
%DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
%LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
%OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
%ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
